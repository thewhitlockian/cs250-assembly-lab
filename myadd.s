	.text
.globl add
	.type	add, @function
add:
	movq	%rdi, %rax  /* moves first argument to return register */
	addq	%rsi, %rax  /* adds second argument to value in return register */
	addq	%rdx, %rax /* adds third argument to value in return register */
	ret /* returns value in %rax */

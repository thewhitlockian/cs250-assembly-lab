	.text
.globl fact
	.type	fact, @function
fact:
	movq	$1 , %rax 		# set return to 1
loop:	

	imulq	%rdi , %rax		# multiply
	decq	%rdi			# subrtract 1 from arg
 
	cmpq	$1, %rdi		# is arg 1?
	je	end			# if so we're done
	jmp	loop			# if not do it again

end:
	cvtsi2sdq %rax, %xmm0		# convert to double
	ret	

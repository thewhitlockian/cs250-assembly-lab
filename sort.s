	.data
	.comm	curr,4		#int curr

	.text
prompt:
	.string "Sort:\n"	
sorted:
	.string "Sorted:\n"	
formatp:
	.string "%d\n"
formats:
	.string "%d"

.globl main

main:
	movq	$0 , %r12		# initialize counter to 0
	movq	$prompt, %rdi		# printf("Sort:\n");
	movq	$0 , %rax		#
	call	printf			#

gather:
	
	movq	$formats, %rdi		# Gets value 
					# scanf("%d",&curr);
	movq	$curr,	%rsi		#  
	movq	$0 ,	%rax		#
	call	scanf			#			
	
	push	%rax			# save return value from scanf on stack

	movq	$curr, %rsi		# store value in %rsi
	movq	(%rsi) , %rsi		# 

	pop	%rax			# return value from scanf

	cmpq	$1 , %rax 		# Did we see an EOF?
	jne	sort			# Jump to sort if we did
	
					# OTHERWISE
	
	push	%rsi			# save last value on stack
	addq	$1, %r12		# add 1 to total number of elements
	jmp 	gather			# return to gather more values
	
sort:
	cmpq	$0 , %r12		# did we actually get any elements?
	je	end			# no, jump to the end

					# otherwise:
	movq	$sorted, %rdi		# printf("Sorted:\n");
	movq	$0 ,	%rax		#
	call	printf			#

	movq	%r12 , %rax		# save number of values for later

sortloop:
	cmpq	$1 , %r12		# only one element left?
	jl	printresults		# base case; we're done

	movq	$-1 , %rcx		# create a counter for inner loop
innerloop:

	incq	%rcx			# increment counter
	cmpq	%rcx , %r12		# compare with outer value
	jle	noswap			# reached the end so exit inner loop
	
	movq	%rcx , %r8		#
	imulq	$8 , %r8		# create real pointer
	addq	%rsp , %r8		# to current value on stack

	movq	%r8 , %r9		#
	addq	$8 , %r9		# and to the next value

	movq	(%r8) , %rsi		#
	movq	(%r9) , %rdi		#
	cmpq	%rsi , %rdi 		# compare values
	jge	innerloop		# if values are OK, dont swap
				
	# otherwise swap values

	movq	%rdi , (%r8)		# overwrite r8 with r9
	movq	%rsi , (%r9)		# copy value from temp to r8
	jmp	innerloop
noswap:
	# finished with innerloop

	decq	%r12			# subtract 1 from r12
	jmp	sortloop	


printresults:
	movq	%rax , %r12		# restore number of values

printloop:
	cmpq	$0 , %r12		# any elements left?
	je	end			# nope, we're done
					# yes

	decq	%r12			# decrement the counter

	pop	%rsi			# pop result to rsi
	movq	$0 , %rax		# empty rax
	movq	$formatp , %rdi		# add the format argument
	call	printf			# printf

	jmp 	printloop		# do it again
	
end:	
	ret

	.text
.globl addarray
	.type	addarray, @function
addarray:
	cmpq	$0 , %rdi	# are there elements left?
	je	end		
	
	addq	(%rsi), %rax
	addq	$4,	%rsi
	decq	%rdi
	jmp	addarray
end:
	ret	

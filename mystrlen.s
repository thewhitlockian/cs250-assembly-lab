    .file   "mystrlen.c"
    .text
.globl mystrlen
    .type   mystrlen, @function
mystrlen:
    movq    $0 , %rax       # set return value to 0

while:
    movq    (%rdi), %rcx    # read bytes from memory
    cmpb    $0 , %cl        # compares value pointed to by first arg to zero
    je      end             # return if value is zero

    addq    $1, %rax        # value isn't zero so add 1 to the main counter
    addq    $1, %rdi        # increment string pointer by 1
    jmp     while           # return to beginning of loop
end:
    ret


